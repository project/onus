<?php

$grid = theme_get_setting('fixed_grid_base');
  
$limiter = '.limiter{width:'.($grid * theme_get_setting('page_column')).'px;}';

$content_width = '#main-content{width:'.($grid * $vars['content_column']).'px;}';

$half = theme_get_setting('fixed_gutter') / 2 . 'px;';
$gutter = '.region{padding-left:'.$half.'padding-right:'.$half.'}';

$left_column = '';
if (theme_get_setting('left_active') == 1) {
  $left_column = '#sidebar-first-outer {width:'.($grid * theme_get_setting('left_column')).'px;}';
}

$right_column = '';
if (theme_get_setting('right_active') == 1) {
  $right_column = '#sidebar-second-outer{width:'.($grid * theme_get_setting('right_column')).'px;}';
}

$left_inner_column = '';
if (theme_get_setting('left_inner_active') == 1) {
  $left_inner_column = '#sidebar-first-inner{width:'.($grid * theme_get_setting('left_inner_column')).'px;}';
}

$right_inner_column = '';
if (theme_get_setting('right_inner_active') == 1) {
  $right_inner_column = '#sidebar-second-inner{width:'.($grid * theme_get_setting('right_inner_column')).'px;}';
}


$styles = 
  $limiter.
  $content_width.
  $gutter.
  $left_column.
  $right_column.
  $left_inner_column.
  $right_inner_column;

drupal_add_css($styles, array('group' => CSS_THEME, 'weight' => 999, 'type' => 'inline'));