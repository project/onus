<?php


/**
 * Calculate & convert
 */
// Fluid page percentage
if ($fluid_page_width >= 1) {
  $fluid_page_width = $fluid_page_width;
}
else {
  $fluid_page_width = 100;
}

// Divide page columns to percentage
if ($page_column > 1) {
  $column_width = 100 / $page_column; // unit = %
}
else {
  $page_column = 16; // Default value
  $column_width = 100; // unit = %
}

// Collect left columns
_else_zero($left_active, $left_column);
_else_zero($left_inner_active, $left_inner_column);
$left_col_total = $left_column + $left_inner_column;

// Collect right columns
_else_zero($right_active, $right_column);
_else_zero($right_inner_active, $right_inner_column);
$right_col_total = $right_column + $right_inner_column;



// #main-content and #sidebar-first columns
$section_wrapper_column = $page_column - $right_col_total; // unit = column

// Convert percentage for CSS
//$section_wrapper_percent = round((100 / $page_column) * $section_wrapper_column, 4); // unit = %
// Use $right_col_total because $content_column can be fixed number
$sidebar_second_percent = round((100 / $page_column) * $right_col_total, 4); // unit = %

$section_wrapper_percent = round(100 - $sidebar_second_percent, 4); // unit = %

// CSS
$sidebar_second_percent_css = '#sidebar-second{width:'.$sidebar_second_percent.'%;}';
$section_wrapper_percent_css = '#section-wrapper{width:'.$section_wrapper_percent.'%;}';

// Percentage for each column
$section_wrapper_column_percent = 100 / $section_wrapper_column;

$sidebar_first_percent = round($left_col_total * $section_wrapper_column_percent, 4);
$sidebar_first_percent_css = '#sidebar-first{width:'.$sidebar_first_percent.'%;}';



$main_content_percent = round(100 - $sidebar_first_percent, 4);


$content_column = $vars['content_column'];
if ($content_fixed == 1) {
  $main_content_percent = round($content_column * $section_wrapper_column_percent, 4);
}

$main_content_percent_css = '#main-content{width:'.$main_content_percent.'%;}';



// Outer and Inner right
$sidebar_second_outer_percent_css = '';
$sidebar_second_inner_percent_css = '';
if ($right_col_total >= 1) {
  $right_col_total_percent = 100 / $right_col_total;
  $sidebar_second_outer_percent = round($right_column * $right_col_total_percent, 4);
  $sidebar_second_inner_percent = round(100 - $sidebar_second_outer_percent, 4);
  $sidebar_second_outer_percent_css = '#sidebar-second-outer{width:'.$sidebar_second_outer_percent.'%;}';
$sidebar_second_inner_percent_css = '#sidebar-second-inner{width:'.$sidebar_second_inner_percent.'%;}';
}

// Outer and Inner left
$sidebar_first_outer_percent_css = '';
$sidebar_first_inner_percent_css = '';
if ($left_col_total >= 1) {
  $left_col_total_percent = 100 / $left_col_total;
  $sidebar_first_outer_percent = round($left_column * $left_col_total_percent, 4);
  $sidebar_first_inner_percent = round(100 - $sidebar_first_outer_percent, 4);
  $sidebar_first_outer_percent_css = '#sidebar-first-outer{width:'.$sidebar_first_outer_percent.'%;}';
$sidebar_second_inner_percent_css = '#sidebar-first-inner{width:'.$sidebar_first_inner_percent.'%;}';
}




// Fixed gutter
$half = theme_get_setting('fixed_gutter') / 2;
$gutter_css = '.region{padding-left:'.$half.'px;padding-right:'.$half.'px;}';

// Min & Max width
$min_width = theme_get_setting('fluid_min_width'); // px
$min_width_css = 'min-width:'.$min_width.'px;';

$max_width = theme_get_setting('fluid_max_width'); // px
$max_width_css = 'max-width:'.$max_width.'px;';



$limiter_css = '.limiter{width:'.$fluid_page_width.'%;margin:auto;}';

if ($min_max_width_enable == 1) {
  $limiter_css = '.limiter{width:'.$fluid_page_width.'%;margin:auto;'.$min_width_css.$max_width_css.'}';
}





$styles = 
  $limiter_css.
  $section_wrapper_percent_css.
  $sidebar_second_percent_css.
  $sidebar_first_percent_css.
  $main_content_percent_css.
  $sidebar_second_outer_percent_css.
  $sidebar_second_inner_percent_css.
  $sidebar_first_outer_percent_css.
  $sidebar_first_inner_percent_css.
  $gutter_css;

drupal_add_css($styles, array('group' => CSS_THEME, 'weight' => 999, 'type' => 'inline'));