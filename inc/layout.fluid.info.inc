<?php
// Temporal Dev messege
drupal_set_message(

  '<ul>Theme seetings data:<br />
    <li>Page width = '. $fluid_page_width . '% Input value = ' . theme_get_setting('fluid_page_width') . '</li>
    <li>Page columns = ' . $page_column . '</li>
    <li>Main content columns = ' . $vars['content_column'] . ' columns</li>
    <li>Outer left sidebar = ' . _else_zero($left_active, $left_column) . ' columns ' . ' Input value = ' . theme_get_setting('left_column') . '</li>
    <li>Inner left sidebar = '._else_zero($left_inner_active, $left_inner_column).' columns ' . ' Input value = ' . theme_get_setting('left_inner_column') . '</li>
    <li>Outer right sidebar = '._else_zero($right_active, $right_column).' columns ' . ' Input value = ' . theme_get_setting('right_column') . '</li>
    <li>Inner right sidebar = '._else_zero($right_inner_active,$right_inner_column).' columns ' . ' Input value = ' . theme_get_setting('right_inner_column') . '</li>
  </ul>',

'warning');