<?php
/**
 * Numeric related variables
 * If variable is not numeric or integer type
 */
function _else_zero($active, $column) {
  if($active == 1 && $column >= 1) {
    return $column;
  }
  else {
    $column = 0;
    return $column;
  }
}


/**
 * Grid Layout Handler for Main Content
 */
function _grid_divide($col1, $col2, $col3, $col4) {

  $count = 0;
  if ($col1) {
    $count++;
  }
  if ($col2) {
    $count++;
  }
  if ($col3) {
    $count++;
  }
  if ($col4) {
    $count++;
  }

  // divide equal width columns
  $width = 80 * theme_get_setting('page_column');
  if ($count > 1) {
    $width = $width / $count;
  }
  return $width;

}

function _content_column($l1, $r1, $l2, $r2) {

  $page = theme_get_setting('page_column');
  
  $left = 0;
  if (theme_get_setting('left_active') == 1 && !empty($l1)) {
    $left = theme_get_setting('left_column');
  }
  
  $right = 0;
  if (theme_get_setting('right_active') == 1 && !empty($r1)) {
    $right = theme_get_setting('right_column');
  }
  
  $left_inner = 0;
  if (theme_get_setting('left_inner_active') == 1 && !empty($l2)) {
    $left_inner = theme_get_setting('left_inner_column');
  }

  $right_inner = 0;
  if (theme_get_setting('right_inner_active') == 1 && !empty($r2)) {
    $right_inner = theme_get_setting('right_inner_column');
  }

  $column = $page;
  if (theme_get_setting('content_fixed') == 0) {
    $column = ($page - ($left + $left_inner + $right_inner + $right));
  }
  else {
    $column = theme_get_setting('content_column');
  }

  return $column;
}