<?php
/**
 * All theme setting variables
 * See onus.info for default values.
 */
$width_style = theme_get_setting('width_style'); // Fixed or Fluid

$fixed_grid_base = intval(theme_get_setting('fixed_grid_base')); // unit = px
// will change to page width instead of grid base
// $fixed_page_width = theme_get_setting('fixed_page_width');

$fluid_page_width = intval(theme_get_setting('fluid_page_width')); // unit = %

$page_column = intval(theme_get_setting('page_column')); // unit = column

$min_max_width_enable = theme_get_setting('min_max_width_enable'); // 1 or 0
$fluid_min_width = intval(theme_get_setting('fluid_min_width')); // unit = px
$fluid_max_width = intval(theme_get_setting('fluid_max_width')); // unit = px

$fixed_gutter = intval(theme_get_setting('fixed_gutter')); // unit = px

$content_fixed = theme_get_setting('content_fixed'); // Auto or Fixed
$content_column = intval(theme_get_setting('content_column')); // unit = column

$left_active = theme_get_setting('left_active'); // 1 or 0
$left_column = intval(theme_get_setting('left_column')); // unit = column

$left_inner_active = theme_get_setting('left_inner_active'); // 0 or 1
$left_inner_column = intval(theme_get_setting('left_inner_column')); // unit = column

$right_active = theme_get_setting('right_active'); // 1 or 0
$right_column = intval(theme_get_setting('right_column')); // unit = column

$right_inner_active = theme_get_setting('right_inner_active'); // 0 or 1
$right_inner_column = intval(theme_get_setting('right_inner_column')); // unit = column

$grid_toggle = theme_get_setting('grid_toggle'); // 0 or 1