(function($) {

  Drupal.behaviors.onus = {
    attach: function(context) {

      // append grid tools to <body>
      $('body').gridToggle();

      // calculate hight every time new page was loaded
      var docHeight = $(document).height();
      $('#gridBG').css({ height: docHeight + 'px' });
    }
  }

})(jQuery);


jQuery.fn.gridToggle = function () {

  // Get data from theme settings
  var widthStyle = Drupal.settings.onusLayout.widthStyle;

  var columns = Drupal.settings.onusLayout.pageColumn;
  
  
  // Fixed width
  if (widthStyle === 'fixed') {
    var gridBase = Drupal.settings.onusLayout.fixedGridBase;
    var pageWidth = gridBase * columns;
    var gutter = Drupal.settings.onusLayout.fixedGutter;
    var unit = 'px';
  }

  // Fluid width
  if (widthStyle === 'fluid') {
    var pageWidth = Drupal.settings.onusLayout.fluidPageWidth;
    var fluidGutter = Drupal.settings.onusLayout.fluidGutter;
    // var gridBase = (100  * columns) / pageWidth;
    var gridBase = 100 / columns;
    //var gutter = (gridBase * pageWidth / columns) * (fluidGutter / 100);
    var gutter = 20//pageWidth * fluidGutter / 100;

    var unit = '%';
  }

  // Create div#gridBG as grid underlay
  var docHeight = jQuery(document).height();
  this.append('<div id="gridBG" />');
  if (widthStyle === 'fluid') {
    jQuery('#gridBG').addClass('fluid');
  }

  jQuery('#gridBG').hide();
  jQuery('#gridBG').css({
    //width: pageWidth + unit,
    marginLeft: '-' + (pageWidth / 2) + unit,
    marginRight: '-' + (pageWidth / 2) + unit,
  });


  // Loop to add grid column markup
  var colMarkup = '<div class="gridBase"><div class="gridCol"></div></div>';
  var gridCol = '';

  for (var i = 1; i <= columns; i++) {
    gridCol = gridCol + colMarkup;
  }
  
  jQuery('#gridBG').append(gridCol);

  jQuery('.gridBase').css({
    width: gridBase + unit,
  });
  
  jQuery('.gridCol').css({
    height: docHeight + 'px',
    marginLeft: (gutter / 2) + unit,
    marginRight: (gutter / 2) + unit,
  });
  
  // Create a#gridToggle as grid overlay trigger
  jQuery('#gridBG').after('<a id="gridToggle" />');

  // #layoutInfo block
  var layoutInfo = 
    pageWidth + unit +' page width, ' + 
    columns + ' columns, ' + 
    gridBase + unit + ' grid base, ' + 
    gutter + unit + ' gutter';
  
  jQuery('#gridBG').prepend('<div id="layoutInfo"><p>' + layoutInfo + '</p></div>');
  
  jQuery('#layoutInfo').css({
    width: pageWidth + unit, 
    marginLeft: '-' + (pageWidth / 2) + unit, 
  });
  
  // Create cookie for first time visitor
  var cookieName = 'gridToggle';
  if (jQuery.cookie(cookieName) === null) {
    var cookieVal = 'hide';
    jQuery.cookie(cookieName, cookieVal);
    jQuery('#gridToggle').html("Show grid");
  }

  // If cookie value is "hide" grid
  if (jQuery.cookie(cookieName) === 'hide') {
    jQuery('#gridToggle').html("Show grid"); 
    jQuery('#gridBG').hide();

    // toggle function
    jQuery('#gridToggle').toggle(function() {
      jQuery(this).html("Hide grid"); 
      jQuery('#gridBG').slideToggle('slow');
      jQuery('#layoutInfo').fadeIn(1000);
      jQuery.cookie(cookieName, 'show');
    }, function() {
      jQuery(this).html("Show grid");
      jQuery('#layoutInfo').fadeOut(1000);
      jQuery('#gridBG').slideToggle('slow');
      jQuery.cookie(cookieName, 'hide');
    });
  }

  // If cookie value is "show" grid
  if (jQuery.cookie(cookieName) === 'show') {
    jQuery('#gridToggle').html("Hide grid");
    jQuery('#gridBG').show();
    
    // toggle function
    jQuery('#gridToggle').toggle(function() {
      jQuery(this).html("Show grid");
      jQuery('#layoutInfo').fadeOut(1000);
      jQuery('#gridBG').slideToggle('slow');
      jQuery.cookie(cookieName, 'hide');
    }, function() {
      jQuery(this).html("Hide grid");
      jQuery('#layoutInfo').fadeIn(1000);
      jQuery('#gridBG').slideToggle('slow');
      jQuery.cookie(cookieName, 'show');
    });
  }

  // Cookie value checking
  //jQuery.cookie(cookieName, null);
  //alert('Cookie value = ' + jQuery.cookie(cookieName));

};

// Simple slide down toggle function
jQuery.fn.slideToggle = function(speed, easing, callback) {
  return this.animate({height: 'toggle'}, speed, easing, callback);  
};