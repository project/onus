(function($) {

  Drupal.behaviors.onus = {
    attach: function(context) {

      // Fluid layout variables from theme settings
      // See template.php function onus_preprocess_html()
      var widthStyle = Drupal.settings.onusLayout.widthStyle;
      var fluidPageWidth = Drupal.settings.onusLayout.fluidPageWidth;
      var pageColumn = Drupal.settings.onusLayout.pageColumn;
      var fluidGridBase = Drupal.settings.onusLayout.fluidGridBase;
      var minMaxWidthEnable = Drupal.settings.onusLayout.minMaxWidthEnable;
      var fluidMinWidth = Drupal.settings.onusLayout.fluidMinWidth;
      var fluidMaxWidth = Drupal.settings.onusLayout.fluidMaxWidth;
      var fixedGutter = Drupal.settings.onusLayout.fixedGutter;
      
      
      // append grid tools to <body>
      //$('body').gridToggle();
      
      $('body').prepend('<div id="gridBGWrapper"><div id="gridBG"></div></div>');
    
      //var docHeight = $(document).height();

      var fluidMargin = (100 - fluidPageWidth) / 2;
  
      $('#gridBG').css({
        //height: docHeight + 'px',
        width: fluidPageWidth + '%',
        margin: '0 ' + fluidMargin + '%',
      });
      
      // If minMaxWidthEnable enabled
      if (minMaxWidthEnable == 1) {
        $('#gridBG').css({
          minWidth: fluidMinWidth + 'px',
          maxWidth: fluidMaxWidth + 'px',
          margin: 'auto',
        });
      }
      
      // Loop to add grid column markup
      var colMarkup = '<div class="gridBase"><div class="gridCol"></div></div>';
      var gridCol = '';

      for (var i = 1; i <= pageColumn; i++) {
        gridCol = gridCol + colMarkup;
      }
      $('#gridBG').append(gridCol);

      $('.gridBase').css({
        //height: docHeight + 'px',
        width: fluidGridBase + '%',
      });
      
      $('.gridCol').css({
        //height: docHeight + 'px',
        marginLeft: (fixedGutter / 2) + 'px',
        marginRight: (fixedGutter / 2) + 'px',
      });
      
      // Create a#gridToggle as grid overlay trigger
      $('#gridBG').after('<a id="gridToggle" />');
      
      // Call function
      $('#gridToggle').gridToggle();
      // Check & Reset cookie
      //alert($.cookie(cookieName));
      //$.cookie(cookieName, null);
      
      // #layoutInfo block
      var layoutInfo = 
        fluidPageWidth + '% page width, ' + 
        pageColumn + ' columns, ' + 
        fluidGridBase + '% grid base, ' + 
        fixedGutter + 'px gutter';
      
      if (minMaxWidthEnable == 1) {
        var layoutInfo = 
          layoutInfo  + ', ' +
          fluidMinWidth + 'px min-width, ' +
          fluidMaxWidth + 'px max-width '
      }
  
      $('#gridBG').prepend('<div id="layoutInfo"><p>' + layoutInfo + '</p></div>');



      $().resizeHeight();

    }
  }

})(jQuery);

jQuery.fn.gridToggle = function () {
  
  // Create cookie for first time visitor
  var cookieName = 'gridToggle';
  if (jQuery.cookie(cookieName) === null) {
    var cookieVal = 'hide';
    jQuery.cookie(cookieName, cookieVal);
    this.html("Show grid");
  }

  // If cookie value is "hide" grid
  if (jQuery.cookie(cookieName) === 'hide') {
    this.html("Show grid"); 
    jQuery('#gridBG').hide();

    // toggle function
    this.toggle(function() {
      jQuery(this).html("Hide grid"); 
      jQuery('#gridBG').slideToggle('slow');
      jQuery('#layoutInfo').fadeIn(1000);
      jQuery.cookie(cookieName, 'show');
    }, function() {
      jQuery(this).html("Show grid");
      jQuery('#layoutInfo').fadeOut(1000);
      jQuery('#gridBG').slideToggle('slow');
      jQuery.cookie(cookieName, 'hide');
    });
  }

  // If cookie value is "show" grid
  if (jQuery.cookie(cookieName) === 'show') {
    this.html("Hide grid");
    jQuery('#gridBG').show();
    
    // toggle function
    this.toggle(function() {
      jQuery(this).html("Show grid");
      jQuery('#layoutInfo').fadeOut(1000);
      jQuery('#gridBG').slideToggle('slow');
      jQuery.cookie(cookieName, 'hide');
    }, function() {
      jQuery(this).html("Hide grid");
      jQuery('#layoutInfo').fadeIn(1000);
      jQuery('#gridBG').slideToggle('slow');
      jQuery.cookie(cookieName, 'show');
    });
  }

};

// Simple slide down toggle function
jQuery.fn.slideToggle = function(speed, easing, callback) {
  return this.animate({height: 'toggle'}, speed, easing, callback);  
};


jQuery.fn.resizeHeight = function () {
  
  var dHeight = jQuery(document).height();
  
  jQuery(window).each(function () {

    jQuery(window).resize(function () {
      var dHeight = jQuery(document).height();
      jQuery('#gridBG').css({height: dHeight + 'px',});
      jQuery('.gridCol').css({height: dHeight + 'px',});
    });

  });
  
  jQuery('#gridBG').css({height: dHeight + 'px',});
  jQuery('.gridCol').css({height: dHeight + 'px',});
  
};