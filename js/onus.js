(function($) {

  Drupal.behaviors.onus = {
    attach: function(context) {

      // append grid tools to <body>
      $('body').gridToggle();

      // calculate hight every time new page was loaded
      var docHeight = $(document).height();
      $('#gridBG').css({ height: docHeight + 'px' });
    }
  }

})(jQuery);


jQuery.fn.gridToggle = function () {

  // Get data from theme settings
  var gridBase = Drupal.settings.onusLayout.fixedGridBase;
  var columns = Drupal.settings.onusLayout.gridColumn;
  var gutter = Drupal.settings.onusLayout.fixedGutter;
  var pageWidth = gridBase * columns;

  // Create div#gridBG as grid underlay
  var docHeight = jQuery(document).height();
  this.append('<div id="gridBG" />');
  jQuery('#gridBG').hide();
  jQuery('#gridBG').css({
    width: pageWidth + 'px', 
    marginLeft: '-' + (pageWidth / 2) + 'px', 
  });

  // Loop to add grid column markup
  var colMarkup = '<div class="gridBase"><div class="gridCol"></div></div>';
  var gridCol = '';

  for (var i = 1; i <= columns; i++) {
    gridCol = gridCol + colMarkup;
  }
  
  jQuery('#gridBG').append(gridCol);

  jQuery('.gridBase').css({
    width: gridBase + 'px',
    height: docHeight + 'px',
  });
  
  jQuery('.gridCol').css({
    height: docHeight + 'px',
    marginLeft: (gutter / 2) + 'px',
    marginRight: (gutter / 2) + 'px',
  });
  
  // Create a#gridToggle as grid overlay trigger
  jQuery('#gridBG').after('<a id="gridToggle" />');

  // #layoutInfo block
  var layoutInfo = 
    pageWidth + 'px page width, ' + 
    columns + ' columns, ' + 
    gridBase + 'px grid base, ' + 
    gutter + 'px gutter';
  
  jQuery('#gridBG').prepend('<div id="layoutInfo"><p>' + layoutInfo + '</p></div>');
  
  jQuery('#layoutInfo').css({
    width: pageWidth + 'px', 
    marginLeft: '-' + (pageWidth / 2) + 'px', 
  });
  
  // Create cookie for first time visitor
  var cookieName = 'gridToggle';
  if (jQuery.cookie(cookieName) === null) {
    var cookieVal = 'hide';
    jQuery.cookie(cookieName, cookieVal);
    jQuery('#gridToggle').html("Show grid");
  }

  // If cookie value is "hide" grid
  if (jQuery.cookie(cookieName) === 'hide') {
    jQuery('#gridToggle').html("Show grid"); 
    jQuery('#gridBG').hide();

    // toggle function
    jQuery('#gridToggle').toggle(function() {
      jQuery(this).html("Hide grid"); 
      jQuery('#gridBG').slideToggle('slow');
      jQuery('#layoutInfo').fadeIn(1000);
      jQuery.cookie(cookieName, 'show');
    }, function() {
      jQuery(this).html("Show grid");
      jQuery('#layoutInfo').fadeOut(1000);
      jQuery('#gridBG').slideToggle('slow');
      jQuery.cookie(cookieName, 'hide');
    });
  }

  // If cookie value is "show" grid
  if (jQuery.cookie(cookieName) === 'show') {
    jQuery('#gridToggle').html("Hide grid");
    jQuery('#gridBG').show();
    
    // toggle function
    jQuery('#gridToggle').toggle(function() {
      jQuery(this).html("Show grid");
      jQuery('#layoutInfo').fadeOut(1000);
      jQuery('#gridBG').slideToggle('slow');
      jQuery.cookie(cookieName, 'hide');
    }, function() {
      jQuery(this).html("Hide grid");
      jQuery('#layoutInfo').fadeIn(1000);
      jQuery('#gridBG').slideToggle('slow');
      jQuery.cookie(cookieName, 'show');
    });
  }

  // Cookie value checking
  //jQuery.cookie(cookieName, null);
  //alert('Cookie value = ' + jQuery.cookie(cookieName));

};

// Simple slide down toggle function
jQuery.fn.slideToggle = function(speed, easing, callback) {
  return this.animate({height: 'toggle'}, speed, easing, callback);  
};