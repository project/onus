<?php


include_once(drupal_get_path('theme', 'onus') . '/inc/layout.handler.inc');

/**
 * Preprocess functions
 */
function onus_preprocess_html(&$vars) {
  
  //include_once(drupal_get_path('theme', 'onus') . '/inc/theme.settings.variables.inc');

  // Add onus class to <body>
  // $vars['classes_array'][] = 'onus';
  
  $js_path = drupal_get_path('theme', 'onus').'/js';
  $css_path = drupal_get_path('theme', 'onus').'/css';
  
  // Make sure that jquery.cookie was called because grid-toggle needs it.
  // See all system libraries at
  // http://api.drupal.org/api/drupal/modules--system--system.module/function/system_library/7
  drupal_add_library('system', 'jquery.cookie');
  
  // Onus layout setting data
  
  // Column amount will be used for both fixed & fluid width style
  $page_column = theme_get_setting('page_column');
  
  // Data for fixed width layout style
  $fixed_grid_base = theme_get_setting('fixed_grid_base');
  $fixed_gutter = theme_get_setting('fixed_gutter');
  
  // Fixed layout style
  if (theme_get_setting('width_style') == 1) {
    $width_style = 'fixed';
    // Data storage for javascript usage
    $onus_layout = array(
      'widthStyle' => $width_style,
      'pageColumn' => $page_column,
      'fixedGridBase' => $fixed_grid_base,
      'fixedGutter' => $fixed_gutter,
    );
  }

  // Data for fluid width layout style
  $fluid_page_width = theme_get_setting('fluid_page_width');
  $fluid_min_width = theme_get_setting('fluid_min_width');
  $fluid_max_width = theme_get_setting('fluid_max_width');
  $min_max_width_enable = theme_get_setting('min_max_width_enable');
  $fluid_grid_base = round(100 / $page_column, 4);
  
    // Fluid layout style
  if (theme_get_setting('width_style') == 0) {
    $width_style = 'fluid';
    // Data storage for javascript usage
    $onus_layout = array(
      'widthStyle' => $width_style,
      'fluidPageWidth' => $fluid_page_width,
      'pageColumn' => $page_column,
      'fluidGridBase' => $fluid_grid_base,
      'minMaxWidthEnable' => $min_max_width_enable,
      'fluidMinWidth' => $fluid_min_width,
      'fluidMaxWidth' => $fluid_max_width,
      'fixedGutter' => $fixed_gutter,
    );
  }

  drupal_add_js(array('onusLayout' => $onus_layout), 'setting');


  if (theme_get_setting('width_style') == 1) {
    
    drupal_add_css($css_path . '/fixed.layout.css', array('group' => CSS_THEME,));
    // Grid tools setting
    if (theme_get_setting('grid_toggle') == 1) {
      drupal_add_css($css_path . '/fixed.grid.tools.css', array('group' => CSS_THEME,));
      drupal_add_js($js_path . '/fixed.grid.tools.js',
        array(
          'type' => 'file',
          'scope' => 'header',
          'weight' => 999,
          'group' => JS_THEME,
          'preprocess' => TRUE,
          'cache' => TRUE,
        )
      );
    }

  }

  if (theme_get_setting('width_style') == 0) {

    drupal_add_css($css_path . '/fluid.layout.css', array('group' => CSS_THEME,));
    // Grid tools setting
    if (theme_get_setting('grid_toggle') == 1) {
      drupal_add_css($css_path . '/fluid.grid.tools.css', array('group' => CSS_THEME,));
      drupal_add_js($js_path . '/fluid.grid.tools.js',
        array(
          'type' => 'file',
          'scope' => 'header',
          'weight' => 999,
          'group' => JS_THEME,
          'preprocess' => TRUE,
          'cache' => TRUE,
        )
      );
    }

  }


}

function onus_preprocess_page(&$vars) {
  
  /* Libraries & LESS CSS DEV
  if (!module_exists('libraries')) {
    drupal_set_message(t('This theme requires <a href="http://drupal.org/project/libraries">Libraries</a> module to be installed.'), 'warning');
  }
  */
  
  include(drupal_get_path('theme', 'onus') . '/inc/theme.settings.variables.inc');


  // Strip <a> tag from Tao
  $vars['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  
  // Always print the site name and slogan, but if they are toggled off, we'll
  // just hide them visually.
  $vars['hide_site_name']   = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $vars['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;

  if ($vars['hide_site_slogan']) {
    // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
    $vars['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }
 
  // sidebars
  $vars['left'] = $vars['page']['left'];
  if ($left_active == 0) {
    $vars['left'] = '';
  }
  if(empty($vars['left'])) {
    $left_column = 0;
  }
  
  $vars['right'] = $vars['page']['right'];
  if ($right_active == 0) {
    $vars['right'] = '';
  }
  if(empty($vars['right'])) {
    $right_column = 0;
  }
  
  $vars['left_inner'] = $vars['page']['left_inner'];
  if ($left_inner_active == 0) {
    $vars['left_inner'] = '';
  }
  if(empty($vars['left_inner'])) {
    $left_inner_column = 0;
  }
  
  $vars['right_inner'] = $vars['page']['right_inner'];
  if ($right_inner_active == 0) {
    $vars['right_inner'] = '';
    $right_inner_column = 0;
  }
  if(empty($vars['right_inner'])) {
    $right_inner_column = 0;
  }
  
  // content
  $vars['content_column'] = _content_column($vars['left'], $vars['right'], $vars['left_inner'], $vars['right_inner']);

  
  // Exhibit regions
  $vars['exhibit'] = $vars['page']['exhibit'];
  $vars['exhibit_1'] = $vars['page']['exhibit_1'];
  $vars['exhibit_2'] = $vars['page']['exhibit_2'];
  $vars['exhibit_3'] = $vars['page']['exhibit_3'];
  $vars['exhibit_4'] = $vars['page']['exhibit_4'];

  $vars['exhibit_col'] = _grid_divide($vars['exhibit_1'], $vars['exhibit_2'], $vars['exhibit_3'], $vars['exhibit_4']) . 'px';
  
  // Supplement regions
  $vars['sup'] = $vars['page']['supplement'];
  $vars['sup_1'] = $vars['page']['supplement_1'];
  $vars['sup_2'] = $vars['page']['supplement_2'];
  $vars['sup_3'] = $vars['page']['supplement_3'];
  $vars['sup_4'] = $vars['page']['supplement_4'];

  $vars['supplement_col'] = _grid_divide($vars['sup_1'], $vars['sup_2'], $vars['sup_3'], $vars['sup_4'])  . 'px';

  if (theme_get_setting('width_style') == 1) {
    include(drupal_get_path('theme', 'onus') . '/inc/layout.fixed.css.inc');
  }
  
  if (theme_get_setting('width_style') == 0) {
    include(drupal_get_path('theme', 'onus') . '/inc/layout.fluid.css.inc');
    include(drupal_get_path('theme', 'onus') . '/inc/layout.fluid.info.inc');
  }



}