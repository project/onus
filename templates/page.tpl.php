<div id='page' class='limiter clearfix'>

<?php if ($page['header']): ?>
  <div id='header' class='section clearfix'>
    <?php print render($page['header']); ?>
  </div>
<?php endif; ?>

<?php if ($logo || $site_name || $site_slogan): ?>
<div id='branding' class='section region clearfix'>

  <?php if ($logo): ?>
    <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
      <img src="<?php print $logo; ?>" alt="<?php print $site_name ?>" />
    </a>
  <?php endif; ?>
    
  <?php if ($site_name || $site_slogan): ?>
    <div id="name-and-slogan"<?php if ($hide_site_name && $hide_site_slogan) { print ' class="element-invisible"'; } ?>>

      <?php if ($site_name): ?>
        <?php if ($title): ?>
          <div id="site-name"<?php if ($hide_site_name) { print ' class="element-invisible"'; } ?>>
            <strong><a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a></strong>
          </div>
        <?php else: /* Use h1 when the content title is empty */ ?>
          <h1 id="site-name"<?php if ($hide_site_name) { print ' class="element-invisible"'; } ?>>
            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a>
          </h1>
        <?php endif; ?>
      <?php endif; ?>

      <?php if ($site_slogan): ?>
        <div id="site-slogan"<?php if ($hide_site_slogan) { print ' class="element-invisible"'; } ?>>
          <?php print $site_slogan; ?>
        </div>
      <?php endif; ?>

    </div> <!-- /#name-and-slogan -->
  <?php endif; ?>

</div> <!-- /#branding -->
<?php endif; ?>

<div id='navigation' class='section region clearfix'>
  <?php if (isset($main_menu)) : ?>
    <?php print theme('links', array('links' => $main_menu, 'attributes' => array('class' => 'links main-menu'))) ?>
  <?php endif; ?>
  <?php if (isset($secondary_menu)) : ?>
    <?php print theme('links', array('links' => $secondary_menu, 'attributes' => array('class' => 'links secondary-menu'))) ?>
  <?php endif; ?>
  
  <?php print $feed_icons ?>

</div> <!-- /#navigation -->

<?php if ($exhibit || $exhibit_1 || $exhibit_2 || $exhibit_3 || $exhibit_4): ?>
<div id='exhibit' class='section clearfix'>

  <?php if ($exhibit): ?>
    <div id='exhibit-top' class='clearfix'>
    <?php print render($exhibit) ?>
    </div>
  <?php endif; ?>

  <?php if ($exhibit_1): ?>
    <div id='exhibit-1' class='exhibit clearfix' style='width: <?php print $exhibit_col ?>;'>
    <?php print render($exhibit_1) ?>
    </div>
  <?php endif; ?>

  <?php if ($exhibit_2): ?>
    <div id='exhibit-2' class='exhibit clearfix' style='width: <?php print $exhibit_col ?>;'>
    <?php print render($exhibit_2) ?>
    </div>
  <?php endif; ?>

  <?php if ($exhibit_3): ?>
    <div id='exhibit-3' class='exhibit clearfix' style='width: <?php print $exhibit_col ?>;'>
    <?php print render($exhibit_3) ?>
    </div>
  <?php endif; ?>

  <?php if ($exhibit_4): ?>
    <div id='exhibit-4' class='exhibit clearfix' style='width: <?php print $exhibit_col ?>;'>
    <?php print render($exhibit_4) ?>
    </div>
  <?php endif; ?>

</div> <!-- /#exhibit -->
<?php endif; ?>

<?php if ($page['help'] || ($show_messages && $messages)): ?>
  <div id='console' class='section clearfix'>
  <?php if ($show_messages && $messages): print '<div class="region clearfix">' . $messages . '</div>'; endif; ?>
  <?php print render($page['help']); ?>
  </div> <!-- /#console -->
<?php endif; ?>

<div id='main' class='clearfix'>

<div id='section-wrapper'>
  <div id='main-content' class='section clearfix'>

    <?php if ($title): ?>
    <div class='title-rigion region clearfix'>
      <?php if ($title): ?><h1 class='page-title'><?php print $title ?></h1><?php endif; ?>
      <?php if ($breadcrumb) print $breadcrumb; ?>

      <?php if ($primary_local_tasks): ?><ul class='links clearfix'><?php print render($primary_local_tasks) ?></ul><?php endif; ?>

      <?php if ($secondary_local_tasks): ?><ul class='links clearfix'><?php print render($secondary_local_tasks) ?></ul><?php endif; ?>

      <?php if ($action_links): ?><ul class='links clearfix'><?php print render($action_links); ?></ul><?php endif; ?>
    </div>
    <?php endif; ?>

    <?php print render($page['content']) ?>

  </div>
  
  <?php if ($left || $left_inner): ?>
    <div id='sidebar-first' class='sidebar clearfix'>
      <?php if ($left): ?>
      <div id='sidebar-first-outer' class='clearfix'><?php print render($left) ?></div>
      <?php endif; ?>
      <?php if ($left_inner): ?>
      <div id='sidebar-first-inner' class='clearfix'><?php print render($left_inner) ?></div>
      <?php endif; ?>
    </div>
  <?php endif; ?>

</div> <!-- /#section-wrapper-->

  <?php if ($right || $right_inner): ?>
    <div id='sidebar-second' class='sidebar clearfix'>
      <?php if ($right_inner): ?>
      <div id='sidebar-second-inner' class='clearfix'><?php print render($right_inner) ?></div>
      <?php endif; ?>
      <?php if ($right): ?>
      <div id='sidebar-second-outer' class='clearfix'><?php print render($right) ?></div>
      <?php endif; ?>
    </div>
  <?php endif; ?>

</div> <!-- /#main -->

<?php if ($sup || $sup_1 || $sup_2 || $sup_3 || $sup_4): ?>
<div id='supplement' class='section clearfix'>

  <?php if ($sup): ?>
    <div id='supplement-top' class='clearfix'>
    <?php print render($sup) ?>
    </div>
  <?php endif; ?>

  <?php if ($sup_1): ?>
    <div id='supplement-1' class='supplement clearfix' style='width: <?php print $supplement_col ?>;'>
    <?php print render($sup_1) ?>
    </div>
  <?php endif; ?>

  <?php if ($sup_2): ?>
    <div id='supplement-2' class='supplement clearfix' style='width: <?php print $supplement_col ?>;'>
    <?php print render($sup_2) ?>
    </div>
  <?php endif; ?>

  <?php if ($sup_3): ?>
    <div id='supplement-3' class='supplement clearfix' style='width: <?php print $supplement_col ?>;'>
    <?php print render($sup_3) ?>
    </div>
  <?php endif; ?>

  <?php if ($sup_4): ?>
    <div id='supplement-4' class='supplement clearfix' style='width: <?php print $supplement_col ?>;'>
    <?php print render($sup_4) ?>
    </div>
  <?php endif; ?>

</div> <!-- /#supplement -->
<?php endif; ?>

<?php if ($page['footer']): ?>
<div id="footer" class='section clearfix'>
  <?php print render($page['footer']) ?>
</div>
<?php endif; ?>

</div> <!--/#page-->
