<?php

function onus_form_validate($form, &$form_state) {

  if ($form_state['values']['name'] == '') {

    form_set_error('name', t('You must select a name for this group of settings.'));

  }

} 

function onus_form_system_theme_settings_alter(&$form, $form_state) {

  $form['theme_settings']['#weight'] = 97;
  $form['theme_settings']['#collapsible'] = TRUE;
  $form['theme_settings']['#collapsed'] = TRUE;

  $form['logo']['#weight'] = 98;
  $form['logo']['#collapsible'] = TRUE;
  $form['logo']['#collapsed'] = TRUE;
  
  $form['favicon']['#weight'] = 99;
  $form['favicon']['#collapsible'] = TRUE;
  $form['favicon']['#collapsed'] = TRUE;

  // Onus layput settings
  $form['layout'] = array(
    '#type' => 'fieldset',
    '#title' => t('Layout settings'),
    '#description' => t('Layout features.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  // Page width settings
  $form['layout']['page'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  // Show current layout info
  $grid = theme_get_setting('fixed_grid_base');
  $column = theme_get_setting('page_column');
  $page_width = t('Your current page width is <strong style="color: green;">') . ($grid * $column) . 'px</strong> ('. $grid . 'px X '. $column . ' columns).';
  
  $form['layout']['page']['width_style'] = array(
    '#type' => 'radios', 
    '#title' => t('Page layout style'),
    '#description' => t('Select page layout style (Default: <strong>Fixed width</strong>). '),// . $page_width,
    '#default_value' => theme_get_setting('width_style'),
    '#options' => array(
      1 => t('<strong>Fixed width</strong>'),
      0 => t('<strong>Fluid width</strong>'),
    ),
  );

  // Column amount which will be used for both fixed & fluid style
  $form['layout']['page']['page_column'] = array(
    '#type' => 'textfield',
    '#title' => t('Grid columns'),
    '#description' => t('Input grid columns for your page (Default: <strong>16 columns</strong>).'),
    '#default_value' => theme_get_setting('page_column'),
    '#size' => 3,
    '#maxlength' => 2,
    '#field_suffix' => 'columns',
    '#required' => TRUE,
  );
  
  // Fixed grid base
  $form['layout']['page']['fixed_grid_base'] = array(
    '#type' => 'textfield', 
    '#title' => t('Fixed grid base'),
    '#description' => t('Input grid base per column (Default: <strong>80px</strong>).'),
    '#default_value' => theme_get_setting('fixed_grid_base'),
    '#size' => 3,
    '#maxlength' => 3,
    '#field_suffix' => 'px',
    '#states' => array(
      'visible' => array(
        'input[name="width_style"]' => array('value' => '1'),
      ),
    ),
  );
  
  // Fluid page width
  $form['layout']['page']['fluid_page_width'] = array(
    '#type' => 'textfield', 
    '#title' => t('Fluid page width'),
    '#description' => t('Input fluid page width (Default: <strong>90%</strong>).'),
    '#default_value' => theme_get_setting('fluid_page_width'),
    '#size' => 3,
    '#maxlength' => 3,
    '#field_suffix' => '%',
    
    '#states' => array(
      'visible' => array(
        'input[name="width_style"]' => array('value' => '0'),
      ),
    ),
  );
  
  // Fixed gutter
  $form['layout']['page']['fixed_gutter'] = array(
    '#type' => 'textfield',
    '#title' => t('Gutter'),
    '#description' => t('Input gutter (Default: <strong>20px</strong>).'),
    '#default_value' => theme_get_setting('fixed_gutter'),
    '#size' => 3,
    '#maxlength' => 2,
    '#field_suffix' => 'px',
  );
  
  // Fluid min & max enable
  $form['layout']['page']['min_max_width_enable'] = array(
    '#type' => 'radios', 
    '#title' => t('Min & Max width'),
    '#description' => t('Enable / Disable min & max width (Default: <strong>Enable</strong>).'),
    '#default_value' => theme_get_setting('min_max_width_enable'),
    '#options' => array(
      1 => t('<strong>Enable</strong>'),
      0 => t('<strong>Disable</strong>'),
    ),
    '#states' => array(
      'visible' => array(
        'input[name="width_style"]' => array('value' => '0'),
      ),
    ),
  );
  
  // Fluid min width
  $form['layout']['page']['fluid_min_width'] = array(
    '#type' => 'textfield', 
    '#title' => t('Minimum width'),
    '#description' => t('Input minimun width for fluid page (Default: <strong>800px</strong>).'),
    '#default_value' => theme_get_setting('fluid_min_width'),
    '#size' => 4,
    '#maxlength' => 4,
    '#field_suffix' => 'px',
    '#states' => array(
      'visible' => array(
        'input[name="width_style"]' => array('value' => '0'),
        'input[name="min_max_width_enable"]' => array('value' => '1'),
      ),
    ),
  );

  // Fluid max width
  $form['layout']['page']['fluid_max_width'] = array(
    '#type' => 'textfield', 
    '#title' => t('Maximum width'),
    '#description' => t('Input maximum width for fluid page (Default: <strong>1200px</strong>).'),
    '#default_value' => theme_get_setting('fluid_max_width'),
    '#size' => 4,
    '#maxlength' => 4,
    '#field_suffix' => 'px',
    '#states' => array(
      'visible' => array(
        'input[name="width_style"]' => array('value' => '0'),
        'input[name="min_max_width_enable"]' => array('value' => '1'),
      ),
    ),
  );

  // Content width settings
  $form['layout']['content'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['layout']['content']['content_fixed'] = array(
    '#type' => 'radios',
    '#title' => t('Content width'),
    '#description' => t('Auto span / Fixed content width (Default: <strong>Auto span</strong> is <strong>recommended</strong>).'),
    '#default_value' => theme_get_setting('content_fixed'),
    '#options' => array(
      0 => t('<strong>Auto span</strong>'),
      1 => t('<strong>Fixed width</strong>'),
    ),
  );
  
  $form['layout']['content']['content_fixed_width'] = array(
    '#type' => 'container',
    '#states' => array(
      'visible' => array(
        'input[name="content_fixed"]' => array('value' => '1'),
      ),
    ),
  );

  $form['layout']['content']['content_fixed_width']['content_column'] = array(
    '#type' => 'textfield',
    '#title' => t('Content columns'),
    '#description' => t('Input fixed content grid columns (Default: <strong>6 columns</strong>).'),
    '#default_value' => theme_get_setting('content_column'),
    '#size' => 2,
    '#maxlength' => 2,
    '#field_suffix' => 'columns',
    '#states' => array(
      'visible' => array(
        'input[name="content_fixed"]' => array('value' => '1'),
      ),
    ),
  );

  // Outer left sidebar
  $form['layout']['left'] = array(
    '#type' => 'fieldset',
    '#title' => t('Left sidebars'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['layout']['left']['outer'] = array(
    '#type' => 'fieldset',
    '#title' => t('Outer left'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['layout']['left']['outer']['left_active'] = array(
    '#type' => 'radios',
    '#description' => t('Disable / Enable outer left sidebar (Default: <strong>Enable</strong>).'),
    '#default_value' => theme_get_setting('left_active'),
    '#options' => array(
      0 => t('<strong>Disable</strong>'),
      1 => t('<strong>Enable</strong>'),
    ),
  );

  $form['layout']['left']['outer']['left_column'] = array(
    '#type' => 'textfield',
    '#title' => t('Outer left width'),
    '#description' => t('Input outer left grid columns (Default: <strong>2 columns</strong>).'),
    '#default_value' => theme_get_setting('left_column'),
    '#size' => 2,
    '#maxlength' => 2,
    '#field_suffix' => 'columns',
    '#states' => array(
      'visible' => array(
        'input[name="left_active"]' => array('value' => '1'),
      ),
    ),
  );
  
  // Inner left sidebar
  $form['layout']['left']['inner'] = array(
    '#type' => 'fieldset',
    '#title' => t('Inner left'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['layout']['left']['inner']['left_inner_active'] = array(
    '#type' => 'radios',
    '#description' => t('Disable / Enable inner left sidebar (Default: <strong>Disable</strong>).'),
    '#default_value' => theme_get_setting('left_inner_active'),
    '#options' => array(
      0 => t('<strong>Disable</strong>'),
      1 => t('<strong>Enable</strong>'),
    ),
  );

  $form['layout']['left']['inner']['left_inner_column'] = array(
    '#type' => 'textfield',
    '#title' => t('Inner left width'),
    '#description' => t('Input inner left grid columns (Default: <strong>2 columns</strong>).'),
    '#default_value' => theme_get_setting('left_inner_column'),
    '#size' => 2,
    '#maxlength' => 2,
    '#field_suffix' => 'columns',
    '#states' => array(
      'visible' => array(
        'input[name="left_inner_active"]' => array('value' => '1'),
      ),
    ),
  );

  // Outer right sidebar
  $form['layout']['right'] = array(
    '#type' => 'fieldset',
    '#title' => t('Right sidebars'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['layout']['right']['outer'] = array(
    '#type' => 'fieldset',
    '#title' => t('Outer right'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['layout']['right']['outer']['right_active'] = array(
    '#type' => 'radios',
    '#description' => t('Disable / Enable outer right sidebar (Default: <strong>Enable</strong>).'),
    '#default_value' => theme_get_setting('right_active'),
    '#options' => array(
      0 => t('<strong>Disable</strong>'),
      1 => t('<strong>Enable</strong>'),
    ),
  );

  $form['layout']['right']['outer']['right_column'] = array(
    '#type' => 'textfield',
    '#title' => t('Sidebar right width'),
    '#description' => t('Input outer right grid columns (Default: <strong>2 columns</strong>).'),
    '#default_value' => theme_get_setting('right_column'),
    '#size' => 2,
    '#maxlength' => 2,
    '#field_suffix' => 'columns',
    '#states' => array(
      'visible' => array(
        'input[name="right_active"]' => array('value' => '1'),
      ),
    ),
  );
  
  // Inner right sidebar
  $form['layout']['right']['inner'] = array(
    '#type' => 'fieldset',
    '#title' => t('Inner right'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  
  $form['layout']['right']['inner']['right_inner_active'] = array(
    '#type' => 'radios',
    '#description' => t('Disable / Enable inner right sidebar (Default: <strong>Disable</strong>).'),
    '#default_value' => theme_get_setting('right_inner_active'),
    '#options' => array(
      0 => t('<strong>Disable</strong>'),
      1 => t('<strong>Enable</strong>'),
    ),
  );

  $form['layout']['right']['inner']['right_inner_column'] = array(
    '#type' => 'textfield',
    '#title' => t('Sidebar right inner width'),
    '#description' => t('Input inner right grid columns (Default: <strong>2 columns</strong>).'),
    '#default_value' => theme_get_setting('right_inner_column'),
    '#size' => 2,
    '#maxlength' => 2,
    '#field_suffix' => 'columns',
    '#states' => array(
      'visible' => array(
        'input[name="right_inner_active"]' => array('value' => '1'),
      ),
    ),
  );

  // Grid toggle settings
  $form['layout']['tools'] = array(
    '#type' => 'fieldset',
    '#title' => t('Development tools'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['layout']['tools']['grid_toggle'] = array(
    '#type' => 'radios',
    '#title' => t('Grid toggle'),
    '#description' => t('Disable / Enable Grid toggle for layout checking (Default: <strong>Disable</strong>).'),
    '#default_value' => theme_get_setting('grid_toggle'),
    '#options' => array(
      0 => t('<strong>Disable</strong>'),
      1 => t('<strong>Enable</strong>'),
    ),
  );

}